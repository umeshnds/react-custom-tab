import * as React from "react";
import { Provider, Flex, Text, Button, Header, List, Table, MenuButton, Input } from "@fluentui/react-northstar";
import { useState, useEffect } from "react";
import { useTeams } from "msteams-react-base-component";
import { app } from "@microsoft/teams-js";
import {
    gridNestedBehavior,
    gridCellWithFocusableElementBehavior,
    gridCellMultipleFocusableBehavior,
} from '@fluentui/accessibility';
import { MoreIcon } from '@fluentui/react-icons-northstar';
/**
 * Implementation of the List content page
 */
const contextMenuItems = ['Add to selection', 'Remove', 'Download'];
function handleRowClick(index) {
    alert(`OnClick on the row ${index} executed.`)
}
// const moreOptionCell = {
//     content: <Button tabIndex={-1} icon={<MoreIcon />} circular text iconOnly title="More options" />,
//     menu: ['Drafts', 'Archive'],
//     onClick: e => {
//        // alert('more option button clicked')
//        console.log("more click")
//         e.stopPropagation()
//     },
// }


function showAlert(e, props) {
    console.log("yaa more click", props, e.target)
    e.stopPropagation();
}

function menuItemClick(e, props) {
    console.log("click item", props)
    console.log("yaa menuItemClick", e.currentTarget);
    e.stopPropagation()
}
function handleMoreClick(wd){
    console.log("handleMoreClick", wd)
}
const moreOptionCell = {
    accessibility: gridCellWithFocusableElementBehavior,
    truncateContent: true,
    content: <MenuButton onMenuItemClick={(e, ...props) => menuItemClick(e, ...props)}
        trigger={<Button icon={<MoreIcon />} content="" aria-label="More options" />}
        menu={[
            'Accept',
            'Reject',
            'Close',
        ]}
        on="click"        
    />
};
const moreActionCell = {
    content: (
        <Flex gap="gap.small" vAlign="center">
            <Button size="small" content="tag 1" />
            <Button size="small" content="tag 2" />
            {/* table layout not support now more content in the cell */}
            {/* <Button tabIndex={-1} icon="edit" circular text iconOnly title="edit tags" /> */}
        </Flex>
    ),
    accessibility: gridCellMultipleFocusableBehavior,
}
const header = {
    key: 'header',
    items: [
        {
            content: 'id',
            key: 'id',
        },
        {
            content: 'Name',
            key: 'name',
        },
        {
            content: 'Picture',
            key: 'pic',
        },
        {
            content: 'Age',
            key: 'action',
        },
        // {
        //     content: 'Tags',
        //     key: 'tags',
        // },
        {
            key: 'more options',
            'aria-label': 'options',
        }
    ]
}

const rowsPlain = [
    {
        key: 1,
        items: [
            {
                content: '1',
                key: '1-1',
            },
            {
                content: 'Roman van von der Longername',
                key: '1-2',
                id: 'name-1',
            },
            {
                content: 'None',
                key: '1-3',
            },
            {
                content: '30 years',
                key: '1-4',
                id: 'age-1',
            },
            // {
            //     key: '1-5',
            //     ...moreActionCell,
            // },
            // {
            //     key: '1-6',
            //     ...moreOptionCell,
            // },
            {
                key: '1-5',
                ...moreOptionCell,
                onClick: () => handleMoreClick(1),
            },
        ],
        onClick: () => handleRowClick(1),
        'aria-labelledby': 'name-1 age-1',
        children: (Component, { key, ...rest }) => (
            <MenuButton menu={contextMenuItems} key={key} contextMenu trigger={<Component {...rest} />} />
        ),
    },
    {
        key: 2,
        items: [
            {
                content: '2',
                key: '2-1',
            },
            {
                content: 'Alex',
                key: '2-2',
            },
            {
                content: 'None',
                key: '2-3',
            },
            {
                content: '1 year',
                key: '2-4',
            },
            // {
            //     key: '2-5',
            //     ...moreActionCell,
            // },
            {
                key: '2-5',
                ...moreOptionCell,
            },
        ],
        onClick: () => handleRowClick(2),
        children: (Component, { key, ...rest }) => (
            <MenuButton menu={contextMenuItems} key={key} contextMenu trigger={<Component {...rest} />} />
        ),
    },
    {
        key: 3,
        items: [
            {
                content: '3',
                key: '3-1',
            },
            {
                content: 'Ali',
                key: '3-2',
            },
            {
                content: 'None',
                key: '3-3',
            },
            {
                content: '30000000000000 years',
                truncateContent: true,
                key: '3-4',
            },
            // {
            //     key: '3-5',
            // },
            {
                key: '3-5',
                ...moreOptionCell,
            },
        ],
        onClick: () => handleRowClick(3),
        children: (Component, { key, ...rest }) => (
            <MenuButton menu={contextMenuItems} key={key} contextMenu trigger={<Component {...rest} />} />
        )
    }
]

const items: any = [
    {
        key: "irving",
        header: "Irving Kuhic",
        content: "Program the sensor to the SAS alarm through the haptic SQL card!"
    },
    {
        key: "dante",
        header: "Dante Schneider",
        content: "The GB pixel is down, navigate the virtual interface!"
    }
];
// export const ListTab = () => {
//     return <List {...listConfig} />;
// };

export const ListTab = () => {

    const [{ inTeams, theme, context }] = useTeams();
    const [entityId, setEntityId] = useState<string | undefined>();
    const [tempId, setTempId] = useState(3);
    useEffect(() => {
        console.log("use effect inTeams")
        if (inTeams === true) {
            app.notifySuccess();
        } else {
            setEntityId("Not in Microsoft Teams");
        }
    }, [inTeams]);

    useEffect(() => {
        console.log("use effect context")
        if (context) {
            setEntityId(context.page.id);
        }
    }, [context]);

    useEffect(() => {
        console.log("use effect once");
        setTimeout(() => {
            const idd = tempId + 1;
            if(rowsPlain.length>3){
                rowsPlain.pop();
            }
            else
            rowsPlain.push({
                key: idd,
                items: [
                    {
                        content: idd.toString(),
                        key: idd + '-1',
                    },
                    {
                        content: 'Ali',
                        key: idd + '-2',
                    },
                    {
                        content: 'None',
                        key: idd + '-3',
                    },
                    {
                        content: idd + ' years',
                        truncateContent: true,
                        key: idd + '4',
                    },
                    // {
                    //     key: '3-5',
                    // },
                    {
                        key: idd + '5',
                        ...moreOptionCell,
                    },
                ],
                onClick: () => handleRowClick(idd),
                children: (Component, { key, ...rest }) => (
                    <MenuButton menu={contextMenuItems} key={key} contextMenu trigger={<Component {...rest} />} />
                )
            });
            setTempId(idd);

        }, 20000);
    }, [tempId]);

    /**
     * The render() method to create the UI of the tab
     */
    return (
        <Provider theme={theme}>
            <Flex fill={true} column styles={{
                padding: ".8rem 0 .8rem .5rem"
            }}>
                <Flex.Item>
                    <Header content="This is your tab" />
                </Flex.Item>
                <Flex.Item>
                    <div>
                        <div>
                            <Text content={entityId} /> <br/>
                            {/* {tempId} */}
                            <div>Auto refresh in every 20 seconds</div>
                        </div>

                        <div >
                            {/* <Button onClick={() => alert("It worked!")}>A sample button</Button> */}
                            {/* <List selectable defaultSelectedIndex={0} items={items} /> */}
                           <div style={{float: 'right'}}> <Input placeholder="Search..." /></div>
                            <Table
                                variables={{
                                    cellContentOverflow: 'none',
                                }}
                                header={header}
                                rows={rowsPlain}
                                aria-label="Nested navigation"
                                accessibility={gridNestedBehavior}
                            />
                            <div style={{float: 'right'}}>
                            <Button onClick={() => alert("Previous!")}>Prev</Button>
                            <Button onClick={() => alert("Next!")}>Next</Button>
                            </div>
                        </div>
                    </div>
                </Flex.Item>
                <Flex.Item styles={{
                    padding: ".8rem 0 .8rem .5rem"
                }}>
                    <Text size="smaller" content="(C) Copyright nds" />
                </Flex.Item>
            </Flex>
        </Provider>
    );
};
